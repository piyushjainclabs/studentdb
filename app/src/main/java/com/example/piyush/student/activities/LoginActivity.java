package com.example.piyush.student.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.example.piyush.student.R;


public class LoginActivity extends ActionBarActivity {
    EditText editUser, editPass;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        editUser = (EditText) findViewById(R.id.userName);
        editPass = (EditText) findViewById(R.id.password);
        preferences = getSharedPreferences("sharedPreference", 0);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login:
                login();
                break;
        }
    }

    private void login() {
        final Dialog dialog = new Dialog(LoginActivity.this);
        String userName = editUser.getText().toString().trim();
        String password = editPass.getText().toString().trim();
        String savedUser = preferences.getString("userName", "");
        String savedPass = preferences.getString("password", "");
        SharedPreferences.Editor editor = preferences.edit();
        if (userName.isEmpty() || password.isEmpty()) {
            dialog.setTitle("Please input all fields.");
            dialog.show();
        } else if (savedUser.equals("") && savedPass.equals("")) {
            editor.putString("userName", userName);
            editor.putString("password", password);
            editor.commit();
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
        } else if (userName.equals(savedUser) && password.equals(savedPass)) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
        } else {
            dialog.setTitle("Enter Correct Credentials");
            dialog.show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

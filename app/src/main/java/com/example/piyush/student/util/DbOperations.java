package com.example.piyush.student.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.example.piyush.student.activities.MainActivity;
import com.example.piyush.student.entities.Student;

public class DbOperations extends AsyncTask<Object, Integer, Integer> {
    ProgressDialog progressBar;
    Context ctx;

    public DbOperations() {
    }

    public DbOperations(Context ctx) {
        this.ctx = ctx;
    }

    @Override
    protected void onPreExecute() {
        progressBar = new ProgressDialog(ctx);
        progressBar.setCancelable(true);
        progressBar.setMessage("Fetching Records ...");
        progressBar.setIndeterminate(false);
        progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressBar.setProgress(0);
        progressBar.setMax(100);
        progressBar.show();
        super.onPreExecute();
    }

    @Override
    protected Integer doInBackground(Object... params) {
        for (int i = 0; i < 101; i++) {
            try {
                Thread.sleep(10);
                onProgressUpdate(i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        DbController controller = new DbController(ctx);
        switch ((String) params[1]) {
            case "delete":
                controller.open();
                controller.delete((Student) params[0]);
                controller.close();
            case "ViewAll":
                controller.open();
                MainActivity.listAdapter.students = controller.getAllData();
                MainActivity.gridAdapter.students = controller.getAllData();
                controller.close();
        }
        return -1;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        progressBar.setProgress(values[0]);
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
        progressBar.dismiss();
        MainActivity.listAdapter.notifyDataSetChanged();
        MainActivity.gridAdapter.notifyDataSetChanged();
    }
}
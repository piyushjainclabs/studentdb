package com.example.piyush.student.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.piyush.student.R;
import com.example.piyush.student.entities.Student;

import java.util.List;

public class TestAdapter extends BaseAdapter {
    public List<Student> students;
    Context ctx;

    public TestAdapter(Context ctx, List<Student> data) {
        this.ctx = ctx;
        this.students = data;
    }

    @Override
    public int getCount() {
        return students.size();
    }

    @Override
    public Object getItem(int position) {
        return students.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.item, null);
        TextView name = (TextView) row.findViewById(R.id.student_name);
        name.setText(students.get(position).getName());
        TextView roll = (TextView) row.findViewById(R.id.student_roll);
        roll.setText("" + students.get(position).getRoll());
        return row;
    }
}

package com.example.piyush.student.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;

import com.example.piyush.student.R;
import com.example.piyush.student.activities.StudentActivity;
import com.example.piyush.student.activities.ViewStudentActivity;
import com.example.piyush.student.adapters.TestAdapter;
import com.example.piyush.student.entities.Student;
import com.example.piyush.student.util.DbOperations;

import static com.example.piyush.student.activities.MainActivity.RESULT_OK;
import static com.example.piyush.student.activities.MainActivity.gridAdapter;
import static com.example.piyush.student.activities.MainActivity.listAdapter;
import static com.example.piyush.student.activities.MainActivity.students;
import static com.example.piyush.student.appConstant.AppConstants.ADD_STUDENT;
import static com.example.piyush.student.appConstant.AppConstants.EDIT_STUDENT;
import static com.example.piyush.student.appConstant.AppConstants.VIEW_STUDENT;

public class ListFragment extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    public static int clickPosition;

    public ListFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static ListFragment newInstance(int sectionNumber) {
        ListFragment fragment = new ListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list, container, false);
        ListView listView = (ListView) rootView.findViewById(R.id.listview);
        listAdapter = new TestAdapter(getActivity().getBaseContext(), students);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                clickPosition = position;
                final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.popup);
                dialog.setTitle(listAdapter.students.get(position).getName());
                Button viewButton = (Button) dialog.findViewById(R.id.view);
                viewButton.setOnClickListener(new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), ViewStudentActivity.class);
                        intent.putExtra("student", listAdapter.students.get(clickPosition));
                        startActivityForResult(intent, VIEW_STUDENT);
                        dialog.dismiss();
                    }
                });

                Button editButton = (Button) dialog.findViewById(R.id.edit);
                editButton.setOnClickListener(new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), StudentActivity.class);
                        intent.putExtra("context", "edit");
                        intent.putExtra("student", listAdapter.students.get(clickPosition));
                        startActivityForResult(intent, EDIT_STUDENT);
                        dialog.dismiss();
                    }
                });

                Button deleteButton = (Button) dialog.findViewById(R.id.delete);
                deleteButton.setOnClickListener(new Button.OnClickListener() {
                    public void onClick(View v) {
                        Student student = listAdapter.students.get(clickPosition);
                        dialog.dismiss();
                        new DbOperations(getActivity()).execute(student, "delete");
                    }
                });
                dialog.show();
            }
        });
        ImageButton add = (ImageButton) rootView.findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.add:
                        Intent intent = new Intent(getActivity(), StudentActivity.class);
                        startActivityForResult(intent, ADD_STUDENT);
                }
            }
        });
        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        switch (requestCode) {
            case ADD_STUDENT:
                if (resultCode == RESULT_OK) {
                    listAdapter.students.add((Student) intent.getSerializableExtra("student"));
                    gridAdapter.students.add((Student) intent.getSerializableExtra("student"));
                }
                break;
            case EDIT_STUDENT:
                if (resultCode == RESULT_OK) {
                    listAdapter.students.remove(clickPosition);
                    listAdapter.students.add(clickPosition, (Student) intent.getSerializableExtra("student"));
                }
                break;
            case VIEW_STUDENT:

        }
        listAdapter.notifyDataSetChanged();
    }
}
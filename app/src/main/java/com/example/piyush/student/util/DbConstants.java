package com.example.piyush.student.util;

/**
 * Created by PIYUSH on 04-02-2015.
 */
public interface DbConstants {

    static final int DATABASE_VERSION = 2;
    static final String DATABASE_NAME = "studentDB";
    static final String TABLE_STUDENT = "student";
    static final String STUDENT_NAME = "name";
    static final String STUDENT_ROLL = "roll";
    static final String STUDENT_BRANCH = "branch";
    static final String STUDENT_ADDRESS = "address";
    static final String STUDENT_PHONE = "phone";
}

package com.example.piyush.student.appConstant;

/**
 * Created by PIYUSH on 27-01-2015.
 */
public interface AppConstants {
    int ADD_STUDENT = 100;
    int EDIT_STUDENT = 200;
    int VIEW_STUDENT = 300;
}

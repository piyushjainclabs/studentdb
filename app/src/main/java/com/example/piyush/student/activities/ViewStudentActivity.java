package com.example.piyush.student.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.piyush.student.R;
import com.example.piyush.student.entities.Student;


public class ViewStudentActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_student);
        Intent intent = getIntent();
        Student student = (Student) intent.getSerializableExtra("student");
        TextView view = (TextView) findViewById(R.id.name);
        view.setText(student.getName());
        view = (TextView) findViewById(R.id.roll);
        view.setText("" + student.getRoll());
        view = (TextView) findViewById(R.id.branch);
        view.setText(student.getBranch());
        view = (TextView) findViewById(R.id.address);
        view.setText(student.getAddress());
        view = (TextView) findViewById(R.id.phone);
        view.setText("" + student.getPhone());
        setResult(RESULT_OK);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_student, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

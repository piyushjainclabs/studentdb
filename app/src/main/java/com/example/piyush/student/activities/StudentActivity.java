package com.example.piyush.student.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.piyush.student.R;
import com.example.piyush.student.entities.Student;
import com.example.piyush.student.util.DbController;


public class StudentActivity extends ActionBarActivity implements View.OnClickListener {
    EditText editName, editRoll, editBranch, editAddress, editPhone;
    Button save;
    String name, rollNo, branch, address, phone;
    Intent intent;
    int originalRoll = 0;
    String KEY = "add";
    Student student;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);
        editName = (EditText) findViewById(R.id.edit_name);
        editRoll = (EditText) findViewById(R.id.edit_roll);
        editBranch = (EditText) findViewById(R.id.edit_branch);
        editAddress = (EditText) findViewById(R.id.edit_address);
        editPhone = (EditText) findViewById(R.id.edit_phone);
        intent = getIntent();
        if (intent.hasExtra("context")) {
            if (intent.getStringExtra("context").equals("edit")) {
                Student student = (Student) intent.getSerializableExtra("student");
                save = (Button) findViewById(R.id.save);
                save.setText("Update");
                editName.setText(student.getName());
                editRoll.setText("" + student.getRoll());
                editRoll.setEnabled(false);
                editBranch.setText(student.getBranch());
                editAddress.setText(student.getAddress());
                editPhone.setText("" + student.getPhone());
                KEY = "edit";
            }
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save:
                save();
                break;
            case R.id.cancel:
                cancel();
                break;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void save() {
        final Dialog dialog = new Dialog(StudentActivity.this);
        int roll;
        name = editName.getText().toString().trim();
        rollNo = editRoll.getText().toString().trim();
        branch = editBranch.getText().toString().trim();
        address = editAddress.getText().toString().trim();
        phone = editPhone.getText().toString().trim();

        if (name.isEmpty() || rollNo.isEmpty() || branch.isEmpty() || address.isEmpty() || phone.isEmpty()) {
            dialog.setTitle("Please input all fields.");
            dialog.show();
        } else if (!name.matches("([a-zA-Z]+ +)*[a-zA-Z]+")) {
            dialog.setTitle("Name Is Invalid");
            dialog.show();
        } else if ((roll = Integer.parseInt(rollNo)) == 0) {
            dialog.setTitle("Roll no. cannot be 0");
            dialog.show();
        } else if (phone.length() != 10) {
            dialog.setTitle("Phone no. must be 10 digits..");
            dialog.show();
        } else {
            student = new Student(name, roll, branch, address, Long.parseLong(phone));
            new DbOperations().execute(student, KEY);

        }
    }

    private void cancel() {
        setResult(RESULT_CANCELED);
        finish();
    }

    public class DbOperations extends AsyncTask<Object, Integer, Integer> {
        ProgressDialog progressBar;

        @Override
        protected void onPreExecute() {
            progressBar = new ProgressDialog(StudentActivity.this);
            progressBar.setCancelable(true);
            progressBar.setMessage("Adding Record ...");
            progressBar.setIndeterminate(false);
            progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressBar.setProgress(0);
            progressBar.setMax(100);
            progressBar.show();
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Object... params) {
            for (int i = 0; i < 101; i++) {
                try {
                    Thread.sleep(10);
                    onProgressUpdate(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            DbController controller = new DbController(StudentActivity.this);
            Integer result;

            if (params[1].equals("add")) {
                controller.open();
                result = (int) controller.insert((Student) params[0]);
                controller.close();
                if (result > 0)
                    return 1;
            } else if (params[1].equals("edit")) {
                controller.open();
                result = controller.update((Student) params[0]);
                controller.close();
                if (result > 0)
                    return 2;
            }
            return -1;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progressBar.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            progressBar.dismiss();
            if (integer == 1) {
                Toast.makeText(StudentActivity.this,
                        "ADDED", Toast.LENGTH_SHORT).show();
                intent.putExtra("student", student);
                setResult(RESULT_OK, intent);
                finish();
            } else if (integer == 2) {
                Toast.makeText(StudentActivity.this,
                        "UPDATED", Toast.LENGTH_SHORT).show();
                intent.putExtra("student", student);
                setResult(RESULT_OK, intent);
                finish();
            } else if (integer == -1) {
                Toast.makeText(StudentActivity.this,
                        "FAILED", Toast.LENGTH_SHORT).show();
                Dialog dialog = new Dialog(StudentActivity.this);
                dialog.setTitle("Roll no. Already Exists..");
                dialog.show();
            }

        }
    }
}

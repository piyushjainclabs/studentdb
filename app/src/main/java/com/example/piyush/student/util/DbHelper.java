package com.example.piyush.student.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static com.example.piyush.student.util.DbConstants.DATABASE_NAME;
import static com.example.piyush.student.util.DbConstants.DATABASE_VERSION;
import static com.example.piyush.student.util.DbConstants.STUDENT_ADDRESS;
import static com.example.piyush.student.util.DbConstants.STUDENT_BRANCH;
import static com.example.piyush.student.util.DbConstants.STUDENT_NAME;
import static com.example.piyush.student.util.DbConstants.STUDENT_PHONE;
import static com.example.piyush.student.util.DbConstants.STUDENT_ROLL;
import static com.example.piyush.student.util.DbConstants.TABLE_STUDENT;

/**
 * Created by PIYUSH on 04-02-2015.
 */
public class DbHelper extends SQLiteOpenHelper {

    public DbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL("CREATE TABLE "
                    + TABLE_STUDENT + "(" + STUDENT_NAME + " TEXT, " + STUDENT_ROLL + " INTEGER PRIMARY KEY," + STUDENT_BRANCH
                    + " TEXT," + STUDENT_ADDRESS + " TEXT," + STUDENT_PHONE
                    + " INTEGER" + ");");
        } catch (Exception e) {
            Log.e("", "");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STUDENT);
        onCreate(db);
    }
}

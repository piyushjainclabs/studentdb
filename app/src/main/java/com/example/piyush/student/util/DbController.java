package com.example.piyush.student.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import com.example.piyush.student.entities.Student;

import java.util.ArrayList;
import java.util.List;

import static com.example.piyush.student.util.DbConstants.DATABASE_NAME;
import static com.example.piyush.student.util.DbConstants.DATABASE_VERSION;
import static com.example.piyush.student.util.DbConstants.STUDENT_ADDRESS;
import static com.example.piyush.student.util.DbConstants.STUDENT_BRANCH;
import static com.example.piyush.student.util.DbConstants.STUDENT_NAME;
import static com.example.piyush.student.util.DbConstants.STUDENT_PHONE;
import static com.example.piyush.student.util.DbConstants.STUDENT_ROLL;
import static com.example.piyush.student.util.DbConstants.TABLE_STUDENT;

/**
 * Created by PIYUSH on 04-02-2015.
 */
public class DbController {
    DbHelper dbHelper;
    SQLiteDatabase db;
    Context context;

    public DbController(Context context) {
        this.context = context;
    }

    public void open() {
        dbHelper = new DbHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
        db = dbHelper.getWritableDatabase();
    }

    public long insert(Student student) {
        try {
            ContentValues values = new ContentValues();
            values.put(STUDENT_NAME, student.getName());
            values.put(STUDENT_ROLL, student.getRoll());
            values.put(STUDENT_BRANCH, student.getBranch());
            values.put(STUDENT_ADDRESS, student.getAddress());
            values.put(STUDENT_PHONE, student.getPhone());
            return db.insert(TABLE_STUDENT, null, values);
        } catch (SQLiteConstraintException e) {
            return -1;

        }
    }

    public int update(Student student) {
        ContentValues values = new ContentValues();
        values.put(STUDENT_NAME, student.getName());
        values.put(STUDENT_BRANCH, student.getBranch());
        values.put(STUDENT_ADDRESS, student.getAddress());
        values.put(STUDENT_PHONE, student.getPhone());
        return db.update(TABLE_STUDENT, values, STUDENT_ROLL + " =? ", new String[]{String.valueOf(student.getRoll())});
    }

    public List<Student> getAllData() {
        List<Student> studentList = new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_STUDENT, null);
        if (cursor.moveToFirst()) {
            do {
                Student student = new Student();
                student.setName(cursor.getString(cursor.getColumnIndex(STUDENT_NAME)));
                student.setRoll(cursor.getInt(cursor.getColumnIndex(STUDENT_ROLL)));
                student.setBranch(cursor.getString(cursor.getColumnIndex(STUDENT_BRANCH)));
                student.setAddress(cursor.getString(cursor.getColumnIndex(STUDENT_ADDRESS)));
                student.setPhone(cursor.getLong(cursor.getColumnIndex(STUDENT_PHONE)));
                studentList.add(student);
            } while (cursor.moveToNext());
        }

        return studentList;
    }

    public void delete(Student student) {
        db.delete(TABLE_STUDENT, STUDENT_ROLL + "=?", new String[]{String.valueOf(student.getRoll())});
    }

    public void close() {
        db.close();
    }
}

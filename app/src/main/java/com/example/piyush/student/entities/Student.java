package com.example.piyush.student.entities;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by PIYUSH on 22-01-2015.
 */
public class Student implements Comparator<Student>, Comparable<Student>, Serializable {
    String name, branch, address;
    int roll;
    long phone;

    public Student() {
    }

    public Student(String name, int roll, String branch, String address, long phone) {
        this.name = name;
        this.roll = roll;
        this.branch = branch;
        this.address = address;
        this.phone = phone;

    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getPhone() {
        return phone;
    }

    public void setPhone(long phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRoll() {
        return roll;
    }

    public void setRoll(int roll) {
        this.roll = roll;
    }

    @Override
    public int compare(Student s1, Student s2) {
        return s1.roll - s2.roll;

    }

    @Override
    public int compareTo(Student student) {
        return this.name.compareTo(student.name);
    }
}
